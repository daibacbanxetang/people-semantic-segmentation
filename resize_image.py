import cv2
from glob import glob


paths = glob('data/people/train/image/*')
# paths = glob('data/people/train/mask/*')
# paths = glob('data/people/valid/image/*')
# paths = glob('data/people/valid/mask/*')

z = 0
for link in paths:
    img = cv2.imread(link, cv2.IMREAD_UNCHANGED)
    img = cv2.resize(img, (224, 224))
    z += 1
    print(z / len(paths) * 100)
    cv2.imwrite('data/people/train/images/' + link.split('/')[-1], img)