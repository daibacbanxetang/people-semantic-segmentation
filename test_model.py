from time import time
import numpy as np
from torch.nn import functional as F

from models.deeplabv3 import DeepLabV3Plus
from dataloaders import transforms
from models.utils import utils
import cv2
import torch 

model = DeepLabV3Plus()


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model = model.to(device=device)

trained_dict = torch.load('model/DeepLabV3Plus_ResNet50.pth', map_location="cpu")['state_dict']
model.load_state_dict(trained_dict, strict=False)
model.eval()

image = cv2.imread('img_test/anh16.jpg')
h, w = image.shape[:2]
X, pad_up, pad_left, h_new, w_new = utils.preprocessing(image, expected_size=320, pad_value=0)

with torch.no_grad():
    mask = model(X)
    mask = mask[..., pad_up: pad_up+h_new, pad_left: pad_left+w_new]
    mask = F.interpolate(mask, size=(h,w), mode='bilinear', align_corners=True)
    mask = F.softmax(mask, dim=1)
    mask = mask[0,1,...].numpy()

mask[np.where(mask > 0.4)] = 255


mask[np.where(mask > 0.4)] = 255
mask = mask.astype('uint8')

mask = cv2.resize(mask, (w, h))
# image = cv2.resize(image, (224, 224))
obj = cv2.bitwise_and(image, image, mask = mask)

im = np.zeros([h, w, 4])
im[:, :, :3] = obj
im[:, :, 3] = mask
im = im.astype('uint8')

path = 'image_save/images.png'
cv2.imwrite(path, im) 
# cv2.imshow('mask', mask.astype('uint8'))
# cv2.imshow('image', image)
# cv2.imshow('obj', obj)
# cv2.waitKey(0)
# cv2.destroyAllWindows()